const Pool = require('pg').Pool
// const pool = new Pool({
//     user: 'odoo',
//     host: '172.17.0.2',
//     database: 'cerco_test',
//     password: 'yourpass',
//     port: 5432
// });
const pool = new Pool({
    user: 'postgres',
    host: 'candidatures.groupecerco.com',
    database: 'cerco_test',
    password: 'cerco@cerco',
    port: 5432
});



const checkToken = (request, response) => {
    const id = request.params.id;
    console.log(request.params);
    if (id !== null) {
        pool.query('SELECT * FROM op_admission WHERE token_genere = $1',[id],(error, results) => {
            if (error) {
                throw error
            }
            console.log(results.rows);
            
            if (results.rows.length !== 0) {
                response.setHeader('Access-Control-Allow-Origin', '*');
                response.status(200).json({status: 200, data: {score1: results.rows[0].score1, score2: results.rows[0].score2, token: id}})
            }else {
                response.setHeader('Access-Control-Allow-Origin', '*');
                response.status(404).json({status: 404, data: 'Not exist'})
            }
        });
    }

}

const checkStat = (request, response) => {
    console.log(request.params)
    const id = request.params.id;
    const game = request.params.game;
    const {score} = request.body;
    
    console.log(score)
   if (id !== null) {
    if (game == 'logique') {
        pool.query('SELECT * FROM op_admission WHERE token_genere = $1',[id],(error, results) => {
            if (error) {
                throw error
            }
            console.log(results.rows);
            if (results.rows.length !== 0) {
                if (results.rows[0].score2 === 0) {
                
                    console.log(results.rows);
                    response.setHeader('Access-Control-Allow-Origin', '*');
                    response.status(200).json({status: 200, data: 'Can game'})
                        
    
                }else {
                    response.setHeader('Access-Control-Allow-Origin', '*');
                    response.status(400).json({status: 400, data: 'already game'})
                }
                
            }
        });   
    } else if(game == 'memory') {
        pool.query('SELECT * FROM op_admission WHERE token_genere = $1',[id],(error, results) => {
                    if (error) {
                        throw error
                    }
            if (error) {
                throw error
            }
            console.log(results.rows);
           if (results.rows.length !== 0) {
            if (results.rows[0].score1 === 0) {
                console.log(results.rows);
                response.setHeader('Access-Control-Allow-Origin', '*');
                response.status(200).json({status: 200, data: 'Can game'});
            }else {
                response.setHeader('Access-Control-Allow-Origin', '*');
                response.status(400).json({status: 400, data: 'already game'})
            }
           }
        });
    } else if(game == 'quiz') {
        pool.query('SELECT * FROM op_admission WHERE token_genere = $1',[id],(error, results) => {
                    if (error) {
                        throw error
                    }
            if (error) {
                throw error
            }
            console.log(results.rows);
           if (results.rows.length !== 0) {
            if (results.rows[0].score3 === 0) {
                console.log(results.rows);
                response.setHeader('Access-Control-Allow-Origin', '*');
                response.status(200).json({status: 200, data: 'Can game'});
            }else {
                response.setHeader('Access-Control-Allow-Origin', '*');
                response.status(400).json({status: 400, data: 'already game'})
            }
           }
        });
    }
   }
}

const insertScore = (request, response) => {
  
    console.log(request.params)
    const id = request.params.id;
    const game = request.params.game;
    const score = request.params.score;
    
    console.log(request.body)
    if (id !== null) {
        if (game == 'logique') {


            pool.query('SELECT * FROM op_admission WHERE token_genere = $1',[id],(error, results) => {
                if (error) {
                    throw error
                }
                console.log(results.rows);
                if (results.rows.length !== 0) {
                    if (results.rows[0].score2 === 0) {
                        pool.query('UPDATE op_admission SET score2 = $1 WHERE token_genere = $2',[score,id],(error, results) => {
                            if (error) {
                                throw error
                            }
                            
                            response.setHeader('Access-Control-Allow-Origin', '*');
                            response.status(200).json({status: 200, data: 'Update Score 1'})
                            
                        });
                    }else {
                        response.setHeader('Access-Control-Allow-Origin', '*');
                        response.status(404).json({status: 404, data: 'Score already exist'})
                    }
                }
            });      
        } else if(game == 'memory') {
            pool.query('SELECT * FROM op_admission WHERE token_genere = $1',[id],(error, results) => {
                if (error) {
                    throw error
                }
                console.log(id);
                console.log(results.rows);
                if (results.rows.length !== 0) {
                    if (results.rows[0].score1 === 0) {
                        pool.query('UPDATE op_admission SET score1 = $1 WHERE token_genere = $2',[score,id],(error, results) => {
                            if (error) {
                                throw error
                            }
                            
                            response.setHeader('Access-Control-Allow-Origin', '*');
                            response.status(200);
                            
                        });
                    }else {
                        response.setHeader('Access-Control-Allow-Origin', '*');
                        response.status(404).json({status: 404, data: 'Score already exist'})
                    }
                }
            });
        } else if(game == 'quiz') {
            pool.query('SELECT * FROM op_admission WHERE token_genere = $1',[id],(error, results) => {
                if (error) {
                    throw error
                }
                console.log(id);
                console.log(results.rows);
                if (results.rows.length !== 0) {
                    if (results.rows[0].score3 === 0) {
                        pool.query('UPDATE op_admission SET score3 = $1 WHERE token_genere = $2',[score,id],(error, results) => {
                            if (error) {
                                throw error
                            }
                            
                            response.setHeader('Access-Control-Allow-Origin', '*');
                            response.status(200);
                            
                        });
                    }else {
                        response.setHeader('Access-Control-Allow-Origin', '*');
                        response.status(404).json({status: 404, data: 'Score already exist'})
                    }
                }
            });
        }
    }
}

module.exports = {
    checkToken,
    insertScore,
    checkStat,
}
