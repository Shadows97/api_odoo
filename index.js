const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const db = require('./queries')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true,}));


app.get("/:id", db.checkToken);
app.post("/:id/:game/:score", db.insertScore);
app.get("/:id/:game", db.checkStat);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port} 🔥`));
